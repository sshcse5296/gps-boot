$(document).ready(function() {
	function initializeMap() {
		var coordinates = {
			lat : 28.501859,
			lng : 77.410320
		};
		var map = new google.maps.Map(document.getElementById('googleMap'), {
			zoom : 10,
			center : coordinates,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		});
		var marker = new google.maps.Marker({
			position : coordinates,
			map : map
		});
	}
	initializeMap();
});