$(document).ready(function() {
	$('.selectpicker').selectpicker({
		size: '6'
	});
	
	var columns=[
        { 
        	"title":"Vehicle",
            "render": function(data,type,row,meta) { 
                     return '<a href="#" class="edit-row" data-toggle="modal" data-target="#vehicle-edit-modal">' + row.vehicleNumber +'</a>';
                   
                 }
        },
        { 
        	"title":"Owner",
        	"data": "ownerUsername"
        },
        { 
        	"title":"Type",
        	"data": "vehicleType"
        },
        { 
        	"title":"State",
        	"data": "vehicleState"
        },
        { 
        	"title":"Speed",
        	"data": "speed"
        },
        { 
        	"title":"Overspeed",
        	"data": "overSpeed"
        },
        { 
        	"title":"Active",
        	"data": "active",
        	'mRender' : function(rData, type, row) {
        		if(row.active){
            		return "<span style='color:green;'>Active</span>"
        		}else{
            		return "<span style='color:red;'>InActive</span>"        			
        		}
        	}
        },
        { 
        	"title":"Added",
        	"data": "added"
        },
        {    
        	"title":"Delete",
            "defaultContent": '<a href="#" class="delete-row"><img src="/img/delete4.png"></a>'
        }
    ]
	
	 
	  var vehicleTable=$('#vehicle-table').DataTable({
	 	    	"columns":columns,
	 	    	"order": [[ 3, "desc" ]],
	 	    	fnCreatedRow: function (nRow, aData, iDataIndex){
	 		    	$(nRow).attr('id',aData.id);
	 		    }
	});

	$.ajax({
		url:"/vehicle/all",
		type:"GET",
		error: function(error) {
			
		},
		success: function(data) {
			vehicleTable.clear().rows.add(data).draw();
		},
		complete: function() {
			
		}
	})

	$("#vehicle-add-save").on('click',function(){
		var formData = $("#vehicle-add-form").serializeToJSON();
		$.ajax({
			url:"/vehicle/save",
			type:"POST",
			data :JSON.stringify(formData),
			dataType: "json",
			contentType: "application/json",
			error: function(error) {
			
			},
			success: function(data) {
				vehicleTable.row.add(data).draw();
			 	$('#vehicle-add-modal').modal('hide');
			},
			complete: function() {
			
			}
		   });
	   });
	
	   $('#vehicle-add-modal').on('hidden.bs.modal', function () {
	 		 $('#vehicle-add-modal form')[0].reset();
	         });

       $('#vehicle-table').on('click', 'a.delete-row', function(e) {
    	            var selectedRow=$(this).parents('tr');
    	            var data = vehicleTable.row(selectedRow).data();
	 	            e.preventDefault();
	 	            $.ajax({
	 	                url: '/vehicle/'+data.id,
	 	                type: "DELETE",
	 	                success: function () {
	 	                    vehicleTable.row(selectedRow).remove().draw();
	 	                },
	 	                error: function (data) {
	 	                    console.log('Error:', data);
	 	                }
	 	            });
	 	    });
	 
       $('#vehicle-table').on('click', 'a.edit-row', function (e) {
    	   var selectedRow=$(this).parents('tr') ;
           var data = vehicleTable.row(selectedRow).data();
           e.preventDefault();
            $("#edit-vehicle-number").val(data.vehicleNumber);
            $("#edit-vehicle-owner").val(data.ownerUsername);
            $("#edit-vehicle-type").val(data.vehicleType);
            $("#edit-vehicle-state").val(data.vehicleState);
            $("#edit-vehicle-speed").val(data.speed);
            $("#edit-vehicle-overspeed").val(data.overSpeed);
            if(data.active){
            	$("#edit-vehicle-active").prop('checked', true);
     	   }else{
     		  $("#edit-vehicle-active").prop('checked', false);
     	   }  
            $("#edit-vehicle-id").val(data.id);
       });

       $("#vehicle-edit-save").click(function () {
    		var formData = $("#vehicle-edit-form").serializeToJSON();
       	 $.ajax({
	                url: '/vehicle/update',
	                type: "PUT",
	                data :JSON.stringify(formData),
	    			dataType: "json",
	    			contentType: "application/json",
	                success: function (response) {
	                	vehicleTable.row("#"+response.id).data( response ).draw();
				 	      $('#vehicle-edit-modal').modal('hide');
	                },
	                error: function (data) {
	                    console.log('Error:', data);
	                }
	            });           
     });
    
    $('input[name="startDate"]').daterangepicker({
           showDropdowns: true,
           drops: "up",
           singleDatePicker: true,
           locale : {
   		   format : 'DD/MM/YYYY'
   		}
   	});
   	
   $('input[name="endDate"]').daterangepicker({
   		   drops: "up",
   		   startDate: moment().add(1, 'years'),
           showDropdowns: true,
           singleDatePicker: true,
           locale: {
   		   format : 'DD/MM/YYYY'
   		},
   	});
   
   jQuery.ajax({
	    type: 'GET',
	    url: 'vehicle/type',
	    dataType: 'json',
	    success: function(data) {
	        jQuery.each(data, function(index, item) {
	        	$('#vehicle-type').append('<option value="' + index + '">' + item + '</option>');
	        	$('#edit-vehicle-type').append('<option value="' + index + '">' + item + '</option>');
	        });
         $('#vehicle-type').selectpicker("refresh");
	     $('#edit-vehicle-type').selectpicker("refresh");
	    },
	    error: function(error) {
	  
	    }
	});
});