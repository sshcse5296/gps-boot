$('document').ready(function(){
	$("#header").load("header.html"); 
	$("#button_01").click(function(){
		var httpRequest=new XMLHttpRequest();
		if(!httpRequest){
			alert("Not initialization");
			return false;
		}
		httpRequest.onreadystatechange = alertContents
	    httpRequest.open('GET', 'home.html',true);
	    httpRequest.send();
	    
	  function alertContents() {
	    if (httpRequest.readyState === XMLHttpRequest.DONE) {
	      if (httpRequest.status === 200) {
	    	  $.ajax({
	    		  url: "js/script.js",
	    		  dataType: 'script',
	    		  success: function() {
	    			  document.getElementsByTagName('html')[0].innerHTML =
	    	    		  httpRequest.responseText;
	    		  }
	    	  });
	      } else {
	        alert('There are a problem with the request');
	      }
	    }
	  }
	});
});