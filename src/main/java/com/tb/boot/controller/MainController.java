package com.tb.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class MainController {
	@RequestMapping("/")
    public String dashboard() {
		 return "dashboard";
    }
	
	@RequestMapping("/vehicle")
    public String vehicle() {
		 return "vehicleV1";
    }
}