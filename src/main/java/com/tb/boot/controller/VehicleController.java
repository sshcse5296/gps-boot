package com.tb.boot.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tb.boot.constant.VehicleType;
import com.tb.boot.domain.entity.Vehicle;
import com.tb.boot.domain.service.VehicleService;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {
	
	   @Autowired
	   protected VehicleService vehicleService;
	   
	   @GetMapping("/type")
	   public  ResponseEntity<VehicleType[]> findVehicleType(){
		   return ResponseEntity.ok().body(VehicleType.values());

       }
	   
	   @GetMapping("/all")
       public  ResponseEntity<List<Vehicle>> findVehicle(){
    	   List<Vehicle> vehicles= vehicleService.findVehicle();
		   return ResponseEntity.ok().body(vehicles);

       }
	   
	   @GetMapping("/{id}")
       public  ResponseEntity<Vehicle> findVehicle(@PathVariable("id") String id){
    	   Vehicle vehicle= vehicleService.findVehicle(id);
		   return ResponseEntity.ok().body(vehicle);

       }
	   
	   @DeleteMapping("/{id}")
       public ResponseEntity<String> deleteVehicle(@PathVariable("id") String id){
		   vehicleService.deleteVehicle(id);
		   return ResponseEntity.ok().body("Vehicle Deleted");
       }
       
	   
	   @PostMapping("/save")
       public ResponseEntity<Vehicle> saveVehicle1(@RequestBody Vehicle vehicle) {
		   vehicle=vehicleService.saveVehicle(vehicle);
		   return ResponseEntity.ok().body(vehicle);
       }
	   

	   @PutMapping("/update")
       public ResponseEntity<Vehicle> updateVehicle(@RequestBody Vehicle vehicle) {
		   vehicleService.updateVehicle(vehicle);
		   return ResponseEntity.ok().body(vehicle);
       }
   
}