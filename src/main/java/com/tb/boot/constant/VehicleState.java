package com.tb.boot.constant;

public enum VehicleState {
	
	IDLE,RUNNING,OVERSPEED,STOP;
	
}
