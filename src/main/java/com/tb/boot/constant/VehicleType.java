package com.tb.boot.constant;

public enum VehicleType {
	
	Sonographic("Sonographic"), Jeep("Jeep"), Truck("Truck"), Trailer("Trailer"), Tempo("Tempo"),
	Tractor("Tractor"), PersonalCar("Personal Car"), Taxi("Taxi"), Bike("Bike"), SchoolBus("School Bus"),
	PublicBus("Public Bus"), Ambulance("Ambulance"), ThreeWheeler("3 Wheeler");
	private String name;

	private VehicleType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
		
}
