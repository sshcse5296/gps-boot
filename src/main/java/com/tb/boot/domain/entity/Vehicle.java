package com.tb.boot.domain.entity;


import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.tb.boot.constant.VehicleState;
import com.tb.boot.constant.VehicleType;

@Document(collection="Vehicle_List")
public class Vehicle {
	
	@Id
	private String id;
	@Field("v_num")
	private String vehicleNumber;
	@Field("o_usr")
	private String ownerUsername;
	@Field("type")
	private VehicleType vehicleType;
	@Field("state")
	private VehicleState vehicleState;
	@Field("active")
	private boolean active;
	@Field("add")
    private Date added;
	@Field("mod")
    private Date updated;
	@Field("spd")
    private Integer speed;
	@Field("o_spd")
    private Integer overSpeed;
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getOwnerUsername() {
		return ownerUsername;
	}
	public void setOwnerUsername(String ownerUsername) {
		this.ownerUsername = ownerUsername;
	}
	public VehicleType getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(VehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}
	public VehicleState getVehicleState() {
		return vehicleState;
	}
	public void setVehicleState(VehicleState vehicleState) {
		this.vehicleState = vehicleState;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getAdded() {
		return added;
	}
	public void setAdded(Date added) {
		this.added = added;
	}
	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public Integer getSpeed() {
		return speed;
	}
	public void setSpeed(Integer speed) {
		this.speed = speed;
	}
	public Integer getOverSpeed() {
		return overSpeed;
	}
	public void setOverSpeed(Integer overSpeed) {
		this.overSpeed = overSpeed;
	}
	
}
