package com.tb.boot.domain.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tb.boot.domain.entity.Vehicle;

public interface VehicleRepository extends MongoRepository<Vehicle,String>{
	

}
