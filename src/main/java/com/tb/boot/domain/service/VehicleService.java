package com.tb.boot.domain.service;

import java.util.List;

import com.tb.boot.domain.entity.Vehicle;

public interface VehicleService {
	
	
	List<Vehicle> findVehicle();
	
	Vehicle findVehicle(String id);
	
	void deleteVehicle(String id);
	
	Vehicle saveVehicle(Vehicle vehicle);
	
	Vehicle updateVehicle(Vehicle vehicle);
	
	
	
	
	

}
