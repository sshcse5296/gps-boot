package com.tb.boot.domain.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.tb.boot.domain.entity.Vehicle;
import com.tb.boot.domain.repository.VehicleRepository;

@Service
public class VehicleServiceImpl implements VehicleService{
	
	@Autowired
	private VehicleRepository vehicleRepository;

	@Override
	public List<Vehicle> findVehicle() {
		return vehicleRepository.findAll();
	}

	@Override
	public Vehicle findVehicle(String id) {
		return vehicleRepository.findById(id).get();
	}

	@Override
	public void deleteVehicle(String id) {
		vehicleRepository.deleteById(id);
	}

	@Override
	public Vehicle saveVehicle(Vehicle vehicle) {
		vehicle.setAdded(new Date());
	    vehicle.setUpdated(new Date());
		return vehicleRepository.save(vehicle);
	}

	@Override
	public Vehicle updateVehicle(Vehicle vehicle) {
		Vehicle existingVehicle=vehicleRepository.findById(vehicle.getId()).get();
	    if(!StringUtils.isEmpty(vehicle.getVehicleNumber())) {
	    	existingVehicle.setVehicleNumber(vehicle.getVehicleNumber());
	    }
	    if(!StringUtils.isEmpty(vehicle.getOwnerUsername())) {
	    	existingVehicle.setOwnerUsername(vehicle.getOwnerUsername());
	    }
	    if(vehicle.getVehicleType()!=null) {
	    	existingVehicle.setVehicleType(vehicle.getVehicleType());
	    }
	    if(vehicle.getVehicleState()!=null) {
	    	existingVehicle.setVehicleState(vehicle.getVehicleState());
	    }
	    if(vehicle.getSpeed()!=null) {
	    	existingVehicle.setSpeed(vehicle.getSpeed());
	    }
	    if(vehicle.getOverSpeed()!=null) {
	    	existingVehicle.setOverSpeed(vehicle.getOverSpeed());
	    }
	    vehicle.setUpdated(new Date());
		return vehicleRepository.save(existingVehicle);
	}

}
